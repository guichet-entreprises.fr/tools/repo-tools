#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys
import os
import os.path
import logging
import re
import codecs
import zipfile
from urllib.parse import urljoin, quote
import requests
import upref
import pymdtools.common as common


# -----------------------------------------------------------------------------
def create_zip(zipfilename, files_or_folder_list, root_path):
    """
    Create a zip file with all file and folders
    Arguments:
        zipfilename {str} -- the zip filename destination
        files_or_folder_list {list} -- all files or folders
    """
    zipfilename = common.set_correct_path(zipfilename)
    logging.info("Create the zip file %s", zipfilename)

    def add_file(filename, ziph, root_path):
        logging.debug("Add the file %s", filename)
        ziph.write(os.path.join(root_path, filename), filename)

    def add_dir(path, ziph, root_path):
        path = os.path.join(root_path, path)
        for root, unused_dirs, files in os.walk(path):
            for file in files:
                filename = os.path.join(root, file)
                rel_filename = os.path.relpath(filename, root_path)
                add_file(rel_filename, ziph, root_path)

    with zipfile.ZipFile(zipfilename, "w", zipfile.ZIP_DEFLATED) as zipf:
        for fname in files_or_folder_list:
            complet_fname = os.path.join(root_path, fname)
            if os.path.isdir(complet_fname):
                add_dir(fname, zipf, root_path)
            else:
                add_file(fname, zipf, root_path)

    return zipfilename

# -----------------------------------------------------------------------------
def upload_gitlab_release(filenames, release_tag):
    """Upload a zipfile as release of this project

    Arguments:
        filenames {str} -- list of filename
        tag {str} -- the tag to upload to
    """
    data_conf = {
        "server": {"label": "url of gitlab server"},
        "project_id": {
            "label": "Unique id of project, available "
                     "in Project Settings/General"
        },
        "private_token": {"label": "login token with permissions "
                                   "to commit to repo"},
    }
    user_data = upref.get_pref(data_conf, "repotools-gitlab")

    server = user_data['server']
    project_id = user_data['project_id']
    private_token = user_data['private_token']

    logging.info("Uploading to %s (id: %s) @ %s",
                 server, project_id, release_tag)

    if not server.endswith('/'):
        server += '/'

    api_url = urljoin(server, "/api/v4/projects/%s/" % project_id)
    auth = {'PRIVATE-TOKEN': private_token}
    uploads = []
    verify = True  # Ignore ssl certificate failures
    logging.info("Uploading %s", filenames)

    for zipfilename in filenames:
        with codecs.open(zipfilename, 'rb') as filehandle:
            rsp = requests.post(urljoin(api_url, 'uploads'),
                                files={'file': filehandle},
                                headers=auth, verify=verify)
            try:
                rsp.raise_for_status()
            except Exception as ex:
                logging.info("Upload of %s failed: %s", zipfilename, ex)
            else:
                uploads.append(rsp.json()['markdown'])

    def fix_markdown(match):
        return "[%s](%s)" % (match.group(1), quote(match.group(2), safe='/:'))

    uploads = [re.sub(r'^\[(.*)\]\((.*)\)$', fix_markdown, u) for u in uploads]

    description = '  \n'.join(uploads)

    # Now we've got the uploaded file info, attach that to the tag
    url = urljoin(
        api_url, 'repository/tags/{t}'.format(t=quote(release_tag, safe='')))
    tag_details = requests.get(url, headers=auth, verify=verify).json()

    method = requests.post
    if 'release' in tag_details and tag_details['release'] is not None:
        description = '  \n'.join(
            (tag_details['release']['description'], description))
        method = requests.put

    rsp = method(url + '/release',
                 data={'description': description},
                 headers=auth, verify=verify)
    try:
        rsp.raise_for_status()
        tagname = rsp.json()['tag_name']
        logging.info("Uploaded %s to tag %s: %s", filenames,
                     tagname, urljoin(server, "tags/%s" % quote(tagname)))

    except Exception as ex:
        logging.info("Setting tag description failed: "
                     "\"%s\" error: %s", description, ex)

# -----------------------------------------------------------------------------
def upload_project_release(folder):
    """Upload list of folder to the project

    Arguments:
        folder_list {list} -- List of folder at the root of the proect
    """
    import gerepotools
    version = gerepotools.__version__

    filenames = []

    def add_file(filename):
        if version not in filename:
            return
        filenames.append(filename)

    common.apply_function_in_folder(folder, add_file, filename_ext=".exe")
    filenames.sort()
    upload_gitlab_release([filenames[-1]], "v" + version)

# -----------------------------------------------------------------------------
def upload_releases_files():
    upload_project_release(os.path.join(__get_this_folder(), "..", "dist"))

###############################################################################
# Find the filename of this file (depend on the frozen or not)
# This function return the filename of this script.
# The function is complex for the frozen system
#
# @return the filename of THIS script.
###############################################################################
def __get_this_filename():
    result = ""

    if getattr(sys, 'frozen', False):
        # frozen
        result = sys.executable
    else:
        # unfrozen
        result = __file__

    return result

###############################################################################
# Find the filename of this file (depend on the frozen or not)
# This function return the filename of this script.
# The function is complex for the frozen system
#
# @return the folder of THIS script.
###############################################################################
def __get_this_folder():
    return os.path.split(os.path.abspath(os.path.realpath(
        __get_this_filename())))[0]


###############################################################################
# Set up the logging system
###############################################################################
def __set_logging_system():
    log_filename = os.path.splitext(os.path.abspath(
        os.path.realpath(__get_this_filename())))[0] + '.log'
    logging.basicConfig(filename=log_filename, level=logging.DEBUG,
                        format='%(asctime)s: %(message)s',
                        datefmt='%m/%d/%Y %I:%M:%S %p')
    console = logging.StreamHandler()
    console.setLevel(logging.INFO)
    # set a format which is simpler for console use
    formatter = logging.Formatter('%(asctime)s: %(levelname)-8s %(message)s')
    # tell the handler to use this format
    console.setFormatter(formatter)
    # add the handler to the root logger
    logging.getLogger('').addHandler(console)

###############################################################################
# Main script call only if this script is runned directly
###############################################################################
def __main():
    # ------------------------------------
    logging.info('Started %s', __get_this_filename())
    logging.info('The Python version is %s.%s.%s',
                 sys.version_info[0], sys.version_info[1], sys.version_info[2])

    upload_releases_files()

    logging.info('Finished')
    # ------------------------------------


###############################################################################
# Call main function if the script is main
# Exec only if this script is runned directly
###############################################################################
if __name__ == '__main__':
    __set_logging_system()
    __main()
