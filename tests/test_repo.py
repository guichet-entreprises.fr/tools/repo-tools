#!/usr/bin/env python
# -*- coding: utf-8 -*-
###############################################################################
# @copyright Copyright (C) Guichet Entreprises - All Rights Reserved
# 	All Rights Reserved.
# 	Unauthorized copying of this file, via any medium is strictly prohibited
# 	Dissemination of this information or reproduction of this material
# 	is strictly forbidden unless prior written permission is obtained
# 	from Guichet Entreprises.
###############################################################################

import logging
import sys
import os
import os.path

import gerepotools.repo as repo
import gerepotools.core as core

def test_extra_files():
    folder = os.path.join(__get_this_folder(), "test_repo", "test01")
    extra_files = [
        os.path.join('../*.conf'),
    ]
    repo.package_folder(folder, "001", extra_files=extra_files)
    # dest = os.path.join(__get_this_folder(), "test_repo", "test01-001.tar.gz")
    # assert os.path.isfile(dest)
    # os.remove(dest)

def test_basename():
    def my_basename(folder):
        folder = core.set_correct_path(folder)
        print(os.path.split(folder))
        if os.path.basename(folder) == '':
            folder = os.path.dirname(folder)
        return os.path.basename(folder)

    test_list = [
        'c:/toto/',
        'c:/toto',
        'c:/toto///',
        'c:/titi/toto/',
        'c:/titi/toto',
    ]

    for folder in test_list:
        assert my_basename(folder) == 'toto'


def test_repo():
    repo.fill_repo_credential(force_renew=False)

def test_package():
    folder = os.path.join(__get_this_folder(), "test_repo", "test01")
    repo.package_folder(folder, "001")
    dest = os.path.join(__get_this_folder(), "test_repo", "test01-001.tar.gz")
    assert os.path.isfile(dest)
    os.remove(dest)
    folder = os.path.join(__get_this_folder(), "test_repo", "test01")
    repo.package_folder(folder, "001", "testT")
    dest = os.path.join(__get_this_folder(), "test_repo", "testT-001.tar.gz")
    assert os.path.isfile(dest)
    os.remove(dest)

def test_release():
    folder = os.path.join(__get_this_folder(), "test_repo", "www")
    assert os.path.isdir(folder)
    result = repo.upload_release(folder, version="1.1.4")
    assert result


# def test_slack():
#     IMPORT SLACK
#     client = slack.WebClient(token=os.environ['SLACK_API_TOKEN'])

#     response = client.chat_postMessage(
#         channel='#random',
#         text="Hello world!")
#     assert response["ok"]
#     assert response["message"]["text"] == "Hello world!"

#     pass

###############################################################################
# Find the filename of this file (depend on the frozen or not)
# This function return the filename of this script.
# The function is complex for the frozen system
#
# @return the filename of THIS script.
###############################################################################
def __get_this_filename():
    result = ""

    if getattr(sys, 'frozen', False):
        # frozen
        result = sys.executable
    else:
        # unfrozen
        result = __file__

    return result

###############################################################################
# Find the filename of this file (depend on the frozen or not)
# This function return the filename of this script.
# The function is complex for the frozen system
#
# @return the folder of THIS script.
###############################################################################
def __get_this_folder():
    return os.path.split(os.path.abspath(os.path.realpath(
        __get_this_filename())))[0]


###############################################################################
# Set up the logging system
###############################################################################
def __set_logging_system():
    log_filename = os.path.splitext(os.path.abspath(
        os.path.realpath(__get_this_filename())))[0] + '.log'
    logging.basicConfig(filename=log_filename, level=logging.DEBUG,
                        format='%(asctime)s: %(message)s',
                        datefmt='%m/%d/%Y %I:%M:%S %p')
    console = logging.StreamHandler()
    console.setLevel(logging.INFO)
    # set a format which is simpler for console use
    formatter = logging.Formatter('%(asctime)s: %(levelname)-8s %(message)s')
    # tell the handler to use this format
    console.setFormatter(formatter)
    # add the handler to the root logger
    logging.getLogger('').addHandler(console)

###############################################################################
# Main script call only if this script is runned directly
###############################################################################
def __main():
    # ------------------------------------
    logging.info('Started %s', __get_this_filename())
    logging.info('The Python version is %s.%s.%s',
                 sys.version_info[0], sys.version_info[1], sys.version_info[2])

    # test_general()
    # test_repo()
    # test_package()
    # test_release()
    # test_basename()
    test_extra_files()

    logging.info('Finished')
    # ------------------------------------


###############################################################################
# Call main function if the script is main
# Exec only if this script is runned directly
###############################################################################
if __name__ == '__main__':
    __set_logging_system()
    __main()
